(defproject momenta "0.1.0"
  :description "backed for momentum"
  :url "http://momentum.com/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.3.0"]
                 [compojure "1.0.4"]
                 [ring/ring-jetty-adapter "1.1.0"]
                 [enlive "1.0.0"]
                 [clj-json "0.5.0"]
                 [ring-json-params "0.1.3"]
                 [postgresql "9.0-801.jdbc4"]
                 [korma "0.3.0-beta9"]
                 [clj-time "0.4.2"]
                 [clj-aws-s3 "0.3.1"]
                 [com.draines/postal "1.8.0"]]
  :ring {:handler momenta.route/app}
  )
