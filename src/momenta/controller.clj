(ns momenta.controller
  (:use [compojure.response]
        [momenta.view]
        [momenta.model]
        [korma.core]
        [postal.core])
  (:require [aws.sdk.s3 :as s3]
            [ring.util.response]))

(def default_radius 320000)
(def bucketname "momentumgeoshare")
(def cred {:access-key "AKIAJXKP4QU3TDJAQ66Q"
           :secret-key "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"})
(def doomsday (java.util.Date. "2038/1/18"))

(declare create-thumbnail)

(defn wrap_auth [handler session params]
  (if (contains? session :auth_user)
    (do
      (handler session params))
    (ring.util.response/status {:body "401"} 401)))

(defn wrap_sharesession_auth [handler session params]
  (let [sessionid (Integer/parseInt (:sharesession params))
        secret (:secret params)
        user (:auth_user session)]
    (if (nil? user)
      (ring.util.response/status {:body "401"} 401)
      (if (or (false? (:isprivate (first (query_sharesession {:id sessionid}))))
           (session_verify (:id user) sessionid))
        (handler session params)
        (if (= (:secret (first (query_sharesession {:id sessionid}))) secret)
          (do
            (insert_session_verify (:id user) sessionid)
            (handler session params))
          (ring.util.response/status {:body "403"} 403))))))

(defn all_users [session params]
  (response_json (query_user)))

(defn get_user [session params]
  (let [{id :id} params]
    (response_json (first (query_user {:id (Integer/parseInt id)})))))

(defn all_resources [session params]
  (response_json (query_resource)))

(defn get_resource [session params]
  (let [{id :id} params]
    (response_json (first (query_resource {:id (Integer/parseInt id)})))))

(defn delete_resource [session params]
  )

(defn delete_resources [session params]
  (let [idlist (map #(Integer/parseInt %) (clojure.string/split (:idlist params) #","))
        userid (:id (:auth_user session))
        candelete? (fn [userid resourceid]
                     (let [resource (first (query_resource {:id resourceid}))
                           session (first (query_sharesession {:id (:session resource)}))]
                       (or (= userid (:creator resource))
                           (= userid (:momenuser_id session)))))
        {can true cannot false} (group-by (partial candelete? userid) idlist)]
    (doseq [rid can] (remove_resource {:id rid}))
    (if (nil? cannot)
      (response_json "success")
      (ring.util.response/status {:body (response_json (into [] cannot))} 400))))

(defn upload_file [file filename]
  (s3/put-object cred bucketname filename file {:cache-control "max-age=2592000"}) ; max-age 30 days
  (s3/generate-presigned-url cred bucketname filename doomsday))

(defn create_resource [session params]
  (let [{:keys [filename contenttype sharesession]} params
        {{tempfile :tempfile} :filecontent} params
        uid (str (java.util.UUID/randomUUID))
        contenturl (upload_file tempfile (str sharesession "_" uid "_" filename))
        thumbtempfile (java.io.File/createTempFile "thumbnail" "momenta")
        makethumbnailstatus (if (= contenttype "image")
                              (create-thumbnail tempfile thumbtempfile 200)
                              nil)
        thumbnailurl (if (= contenttype "image")
                       (upload_file thumbtempfile (str "thumbnail" sharesession "_" uid "_" filename))
                       "")
        creator (:id (:auth_user session))
        sharesession (Integer/parseInt sharesession)]
    (response_json (insert_resource
                    filename
                    contenttype
                    contenturl
                    thumbnailurl
                    sharesession
                    creator
                    uid))))

(defn all_sharesessions [session params]
  (response_json (query_sharesession)))

(defn get_sharesession [session params]
  (let [{id :id} params]
    (response_json (first (query_sharesession {:id (Integer/parseInt id)})))))

(defn get_sharesession_resources [session params]
  (let [sessionid (:sharesession params)]
    (response_json (group-by :contenttype (query_resource {:session (Integer/parseInt sessionid)})))))

(defn search_nearby_sharesessions [session params]
  (let [radius (Double/parseDouble (or (:radius params) "360"))
        latitude (Double/parseDouble (:latitude params))
        longitude (Double/parseDouble (:longitude params))
        offset (Integer/parseInt (or (:offset params) "0"))
        limit (Integer/parseInt (or (:limit params) "50"))
        sharesessions (map #(dissoc % :secret :password) (search_sharesessions_by_location radius latitude longitude (java.sql.Timestamp. (.getTime (java.util.Date.))) limit offset))]
    (response_json sharesessions)))

(defn create_sharesession [session params]
  (let [expiredate (long (* 1000 (Long/parseLong (:expiredate params)))) ;; java api requires milliseconds after epoch
        createdate (* 1000 (long (/ (.getTime (java.util.Date.)) 1000)))
        momenuser_id (:id (:auth_user session))
        longitude (Double/parseDouble (:longitude params))
        latitude (Double/parseDouble (:latitude params))
        isprivate (not (= (:secret params) ""))
        params (dissoc (assoc params
                         :expiredate expiredate
                         :createdate createdate
                         :momenuser_id momenuser_id
                         :longitude longitude
                         :latitude latitude
                         :isprivate isprivate
                         )
                       :creator)
        newsession (insert_sharesession params)]
    (insert_session_verify momenuser_id (:id newsession))
    (response_json {:id (:id newsession)})))

(defn- queued_remove_sharesession [id]
  (remove_resource {:session id})
  (remove_session_verify {:sessionid id})
  (remove_sharesession {:id id}))

(defn delete_sharesessions [session params]
  (let [idlist (map #(Integer/parseInt %) (clojure.string/split (:idlist params) #","))
        userid (:id (:auth_user session))
        candelete? (fn [userid sharesessionid]
                     (let [sharesession (first (query_sharesession {:id sharesessionid}))]
                       (= userid (:momenuser_id sharesession))))
        {can true cannot false} (group-by (partial candelete? userid) idlist)]
    (doseq [sid can] (queued_remove_sharesession sid))
    (if (nil? cannot)
      (response_json "success")
      (ring.util.response/status {:body (response_json (into [] cannot))} 400))))

(defn- authenticate_user [identity password]
  (let [user (first (query_user {:identity identity}))]
    (cond (nil? user) :nouser
          (= (:password user) password) true
          :else false)))

(defn- confirm_new_user [email]
  (send-message ^{:user "AKIAILKQOCK4CE4GSABQ" :pass "AjgT9uzy+zG5llUwpHk/PozQB8Kb+NsvZH+0rfB5N3tl" :host "email-smtp.us-east-1.amazonaws.com" :port 587 } {:from "momentum@wuyaah.com" :to email :subject "Hello from Momentum" :body "Thanks for registering with Momentum. Enjoy using the Momentum app"}))

(defn- login_user
  "login a user or create a user entity if the identity does not exist"
  [identity password]
  (let [authstatus (authenticate_user identity password)]
    (cond (= authstatus :nouser) (do
                                   (insert_user identity password)
                                   (confirm_new_user [identity])
                                   true)
          :else authstatus)))

(defn user_login [session params]
  (let [identity (:identity params)
        password (:password params)
        status (login_user identity password)]
    (assoc (render (response_json (if (= status true)
                                    {:result :success}
                                    {:result :failure})) nil)
      :session (if (= status true)
                 (let [user (first (query_user {:identity identity}))]
                   (assoc session :auth_user user))
                 session))))

(defn user_isloggedin [session params]
  (if (contains? session :auth_user)
    (response_json true)
    (response_json false)))

(defn verify_session_access [session params]
  (let [id (Integer/parseInt (:sharesessionid params))
        secret (:secret params)
        sharesession (first (query_sharesession {:id id}))]
    (if (= secret (:secret sharesession))
      (response_json {:result "verified"
                      :id id
                      :secret secret})
      (response_json {:result "failure"
                      :id id}))))

(defn verified_sessions [session params]
  (let [userid (:id (:auth_user session))
        timestampnow (java.sql.Timestamp. (.getTime (java.util.Date.)))
        verifiedsessions (exec-raw ["select momensession.id, momenuser.username as creatorname, sessionname, longitude, latitude, expiredate, createdate, isprivate from momensession inner join momenuser on momensession.momenuser_id = momenuser.id where momensession.id in (select sessionid from momensessionverify where userid = ?) and expiredate > ?" [userid timestampnow]] :results)
        convert-date #(assoc %1 %2 (long (/ (.getTime (%2 %1)) 1000)))
        convert-tude #(assoc %1 %2 (.doubleValue (%2 %1)))]
    (response_json (map #(->
                          (convert-date % :expiredate)
                          (convert-date :createdate)
                          (convert-tude :longitude)
                          (convert-tude :latitude)) verifiedsessions))))

(defn create-thumbnail [file new-file imagesize]
  (let [img (javax.imageio.ImageIO/read file)
        imgtype (java.awt.image.BufferedImage/TYPE_INT_ARGB)
        width (.getWidth img)
        height (.getHeight img)
        cropsize (min width height)
        x (/ (- width cropsize) 2)
        y (/ (- height cropsize) 2)
        imgcropfilter (java.awt.image.CropImageFilter. x y cropsize cropsize)
        imgcropproducer (java.awt.image.FilteredImageSource. (.getSource img) imgcropfilter)
        cropimage (.createImage (java.awt.Toolkit/getDefaultToolkit) imgcropproducer)
        imgscalefilter (java.awt.image.ReplicateScaleFilter. imagesize imagesize)
        imgscaleproducer (java.awt.image.FilteredImageSource. (.getSource cropimage) imgscalefilter)
        scaleimage (.createImage (java.awt.Toolkit/getDefaultToolkit) imgscaleproducer)
        simg (java.awt.image.BufferedImage. imagesize imagesize imgtype)
        g (.createGraphics simg)]
    (.drawImage g scaleimage 0 0 imagesize imagesize nil)
    (.dispose g)
    (javax.imageio.ImageIO/write simg "png" new-file)))