(ns momenta.view
;;;  (:use [momenta.model])
  (:require [clj-json.core :as json]))

(defn response_json [obj]
  (json/generate-string obj))
