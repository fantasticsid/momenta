(ns momenta.route
  (:use [compojure.core]
        [momenta.controller]
        [ring.middleware.json-params])
  (:require [compojure.route :as route]
            [compojure.handler :as handler]
            [momenta.middleware]))

(def session-expire-time (* 3600 24 30))

(defroutes main-routes
  ;; route for users
  (GET "/users.json" {session :session params :params} (all_users session params))
  (GET "/user/:id.json" {session :session params :params} (get_user session params))

  ;; route for resources
  (GET "/resources.json" {session :session params :params} (all_resources session params))
  (GET "/resource/:id.json" {session :session params :params} (get_resource session params))
  (DELETE "/resource/:id.json" {session :session params :params} (wrap_auth delete_resource session params))
  (POST "/deleteresources.json" {session :session params :params} (wrap_auth delete_resources session params))
  (POST "/resources.json" {session :session params :params} (wrap_auth create_resource session params))

  ;; route for sharesessions
  (GET "/search/lookup.json" {session :session params :params} (search_nearby_sharesessions session params))
  (GET "/sharesessions/getresources.json" {session :session params :params} (wrap_sharesession_auth get_sharesession_resources session params))
  (POST "/sharesessions.json" {session :session params :params} (wrap_auth create_sharesession session params))
  (POST "/deletesharesessions.json" {session :session params :params} (wrap_auth delete_sharesessions session params))
  (GET "/sharesessions/verifiedsessions.json" {session :session params :params} (verified_sessions session params))

  ; user login
  (POST "/login.json" {session :session params :params} (user_login session params))
  (GET "/isloggedin.json" {session :session params :params} (user_isloggedin session params))

  ;; misc
  (GET "/accesssession/verify.json" {session :session params :params} (verify_session_access session params))
  (GET "/live.html" {session :session params :params} "live")
  (route/files "/public"))

(def app
     (handler/site main-routes {:session {:store (momenta.middleware/table-store)
                                          :cookie-attrs {:max-age session-expire-time}}}))
