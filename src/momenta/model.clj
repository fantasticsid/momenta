(ns momenta.model
  (:use [korma.core]
        [korma.db]))

(defdb db (postgres {:db "momenta"
                     :user "sid"
                     :password "sid"}))

(defentity momenuser
  (database db))

(defentity momensession
  (database db)
  (transform #(-> %
                  (assoc :expiredate (long (/ (.getTime (:expiredate %)) 1000)))
                  (assoc :createdate (long (/ (.getTime (:createdate %)) 1000)))
                  (assoc :latitude (.doubleValue (:latitude %))) ;; since clj-json does not handle bigdecimal
                  (assoc :longitude (.doubleValue (:longitude %)))))
  (prepare #(-> %
                (assoc :expiredate (java.sql.Timestamp. (:expiredate %)))
                (assoc :createdate (java.sql.Timestamp. (:createdate %)))
                ))
  (belongs-to momenuser))

(defentity momenresource
  (database db))

(defentity momensessionverify
  (database db))

(defentity momenringsession
  (database db))

(defn query_user
  ([]
     (select momenuser))
  ([criteria]
     (select momenuser (where criteria))))

(defn insert_user [identity password]
  (insert momenuser (values
                     {:username identity
                      :password password
                      :email identity
                      :identity identity})))

(defn query_resource
  ([]
     (select momenresource))
  ([criteria]
     (select momenresource (where criteria))))

(defn insert_resource [filename contenttype contenturl thumbnailurl session creator uid]
  (insert momenresource (values
                         {:filename filename
                          :contenttype contenttype
                          :contenturl contenturl
                          :thumbnailurl thumbnailurl
                          :session session
                          :creator creator
                          :uid uid})))

(defn remove_resource [criteria]
  (delete momenresource (where criteria)))

(defn query_sharesession
  ([]
     (select momensession (with momenuser)))
  ([criteria]
     (select momensession (with momenuser) (where criteria))))

(defn search_sharesessions_by_location [radius latitude longitude timestamp lmt ost]
  (let [sharesessions (exec-raw ["select createdate, longitude, latitude, username as creatorname, sessionname, expiredate, momensession.id, isprivate, sqrt((longitude - ?)^2 + (latitude - ?)^2) as distance from momensession inner join momenuser on momensession.momenuser_id = momenuser.id where expiredate > ? and sqrt((longitude - ?)^2 + (latitude - ?)^2) <= ? order by distance, createdate desc limit ? offset ?" [longitude, latitude, timestamp, longitude, latitude, (* radius radius), lmt, ost]] :results)
        convert-date #(assoc %1 %2 (long (/ (.getTime (%2 %1)) 1000)))
        convert-tude #(assoc %1 %2 (.doubleValue (%2 %1)))]
    (map #(->
           (convert-date % :expiredate)
           (convert-date :createdate)
           (convert-tude :longitude)
           (convert-tude :latitude)) sharesessions)))

(defn insert_sharesession [params]
  (insert momensession (values params)))

(defn remove_sharesession [criteria]
  (delete momensession (where criteria)))

(defn session_verify [userid sessionid]
  (first (select momensessionverify
                           (where {:sessionid sessionid
                                   :userid userid}))))

(defn insert_session_verify [userid sessionid]
  (insert momensessionverify (values
                              {:sessionid sessionid
                               :userid userid})))

(defn query_verified_session [userid]
  (select momensessionverify
          (where {:userid userid})))

(defn remove_session_verify [criteria]
  (delete momensessionverify (where criteria)))

(defn insert_ringsession [params]
  (insert momenringsession (values params)))

(defn remove_ringsession [criteria]
  (delete momenringsession (where criteria)))

(defn query_ringsession
  ([]
     (select momenringsession))
  ([criteria]
     (select momenringsession (where criteria))))

(defn update_ringsession [criteria newsession]
  (update momenringsession
          (set-fields newsession)
          (where criteria)))
