(ns momenta.middleware
  (:use [ring.middleware.session.store]
        [momenta.model])
  (:require [clj-json.core :as json])
  (:import java.util.UUID))

(defn keyrify-map [m]
  (if (map? m)
    (into {}
          (for [[k v] m]
            (if (string? k)
              [(keyword k) (keyrify-map v)]
              [k (keyrify-map v)])))
    m))

(deftype TableStore []
  SessionStore
  (read-session [_ key]
                (->
                 (query_ringsession {:cookieid key})
                 first
                 (:content "{}")
                 json/parse-string
                 keyrify-map))
  (write-session [_ key data]
                 (let [key (or key (str (UUID/randomUUID)))
                       isnew? (= 0 (count (query_ringsession {:cookieid key})))
                       jsondata (json/generate-string data)]
                   (if isnew?
                     (insert_ringsession {:cookieid key
                                          :content jsondata})
                     (update_ringsession {:cookieid key}
                                         {:content jsondata}))
                   key))
  (delete-session [_ key]
                  (remove_ringsession {:cookieid key})))

(defn table-store
  "Creates a database powered session engine"
  []
  (TableStore.))
