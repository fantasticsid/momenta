(ns momenta.core
  (:use [ring.adapter.jetty]
        [momenta.route]))

(defonce server (run-jetty #'momenta.route/app {:port 8000 :join? false}))
; (.stop server)
; (.start server)
