(ns momenta.model-test
  (:use clojure.test
        momenta.model))

(deftest test_query_user
  (is (= (first (query_user {:id -1})) nil))
  (is (= (first (query_user {:username "*thereshouldntbesuchauser"})) nil))
  (is (or (= (:identity (first (query_user {:identity "cockneykevin@gmail.com"})))
             "cockneykevin@gmail.com")
          (= (first (query_user {:identity "cockneykevin@gmail.com"}))
             nil))))
