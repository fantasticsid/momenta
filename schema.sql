create table momenuser (
id serial primary key,
username text not null,
password text not null,
email text,
identity text not null,
constraint identity unique(identity));

create table momensession (
id serial primary key,
sessionname text,
longitude numeric,
latitude numeric,
secret text,
expiredate timestamp,
createdate timestamp,
isprivate boolean,
momenuser_id integer references momenuser (id));

create table MOMENRESOURCE (
id serial primary key,                 filename text,
contenttype text,
contenturl text,
thumbnailurl text,
uid text,
session integer references momensession (id), creator integer references momenuser (id)
);

create table momensessionverify (
    id serial primary key,
    sessionid integer references momensession (id),
    userid integer references momenuser (id)
);

create table momenringsession (
    id serial primary key,
    cookieid varchar,
    content text,
    constraint cookieid unique(cookieid)
);
